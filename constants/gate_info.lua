local M = {}

M.AND = { type = 0, displayName = "and" }
M.OR  = { type = 1, displayName = "or" }
M.NOT = { type = 2, displayName = "not" }
M.XOR = { type = 3, displayName = "xor" }

M.NAND = { type = 4, displayName = "nand" }
M.NOR  = { type = 5, displayName = "nor" }
M.XNOR = { type = 6, displayName = "xnor" }

return M