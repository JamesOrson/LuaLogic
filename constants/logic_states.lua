local M = {}

M.TRUE = 1
M.FALSE = 0
M.UNKNOWN = 2

return M