local M = {}

M.GATES = 0
M.WIRES = 1
M.CIRCUIT_INFO = 2
M.UNKNOWN = 3

return M