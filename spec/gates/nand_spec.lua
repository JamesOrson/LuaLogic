local GateHelper = require "helpers.gate_helper"
local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"
local Wire = require "gates.wire"

local Nand = require "gates.nand"

local maxNumberOfInputs = 10

describe(
    "Nand gate -",
    function()
        describe(
            "Functions - ",
            function()
                it(
                    "Nand() should call initalize()",
                    function()
                        spy.on(Nand, "initialize")
                        local nandGate = Nand(2, 1)

                        assert.spy(Nand.initialize).was.called()
                    end
                )
                it(
                    "initalize() should set fields",
                    function()
                        local numberOfInputs = 2
                        local delay = 1
                        local nandGate = Nand(numberOfInputs, delay)
                        
                        assert.equals(#nandGate.inputs, 0)
                        assert.equals(#nandGate.outputs, 0)
                        assert.equals(nandGate.delay, delay)
                        assert.equals(nandGate.numberOfInputs, numberOfInputs)
                        assert.equals(nandGate.type, GateInfo.NAND.type)
                    end
                )
                it(
                    "do_logic() should call simulate_logic()",
                    function()
                        local nandGate = Nand(2, 1)
                        GateHelper.add_input_wire(nandGate, Wire(1, LogicStates.UNKNOWN))
                        GateHelper.add_input_wire(nandGate, Wire(2, LogicStates.UNKNOWN))
                        GateHelper.add_output_wire(nandGate, Wire(3, LogicStates.UNKNOWN))
                        spy.on(nandGate, "simulate_logic")
                        nandGate:do_logic()
                
                        assert.spy(nandGate.simulate_logic).was.called()
                    end
                )
            end
        )
        describe(
            "logic -",
            function()
                it(
                    "should have true outputs if any input is false",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local nandGate = Nand(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(nandGate, Wire(j, LogicStates.TRUE))
                            end
                            GateHelper.add_input_wire(nandGate, Wire(i, LogicStates.FALSE))
                            GateHelper.add_output_wire(nandGate, Wire(i + 1, LogicStates.UNKNOWN))

                            nandGate:do_logic()
                            assert.is_true(nandGate.outputs[1].state == LogicStates.TRUE)
                        end
                    end
                )
                it(
                    "should have false outputs if all inputs are true",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local nandGate = Nand(i, 1)
                            for j = 1, i do
                                GateHelper.add_input_wire(nandGate, Wire(j, LogicStates.TRUE))
                            end
                            GateHelper.add_output_wire(nandGate, Wire(i + 1, LogicStates.UNKNOWN))

                            nandGate:do_logic()
                            assert.is_true(nandGate.outputs[1].state == LogicStates.FALSE)
                        end
                    end
                )
                it(
                    "should have true outputs if any input is false, even if other inputs are unknown",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local nandGate = Nand(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(nandGate, Wire(j, LogicStates.UNKNOWN))
                            end
                            GateHelper.add_input_wire(nandGate, Wire(i, LogicStates.FALSE))
                            GateHelper.add_output_wire(nandGate, Wire(i + 1, LogicStates.UNKNOWN))

                            nandGate:do_logic()
                            assert.is_true(nandGate.outputs[1].state == LogicStates.TRUE)
                        end
                    end
                )
                it(
                    "should have unknown outputs if any input is unknown and all other inputs are true",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local nandGate = Nand(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(nandGate, Wire(j, LogicStates.TRUE))
                            end
                            GateHelper.add_input_wire(nandGate, Wire(i, LogicStates.UNKNOWN))
                            GateHelper.add_output_wire(nandGate, Wire(i + 1, LogicStates.UNKNOWN))

                            nandGate:do_logic()
                            assert.is_true(nandGate.outputs[1].state == LogicStates.UNKNOWN)
                        end
                    end
                )
            end
        )
    end
)
