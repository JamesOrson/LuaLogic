local GateHelper = require "helpers.gate_helper"
local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"
local Wire = require "gates.wire"

local Nor = require "gates.nor"

local maxNumberOfInputs = 10

describe(
    "Nor gate -",
    function()
        describe(
            "Functions - ",
            function()
                it(
                    "Nor() should call initalize()",
                    function()
                        spy.on(Nor, "initialize")
                        local norGate = Nor(2, 1)

                        assert.spy(Nor.initialize).was.called()
                    end
                )
                it(
                    "initalize() should set fields",
                    function()
                        local numberOfInputs = 2
                        local delay = 1
                        local norGate = Nor(numberOfInputs, delay)
                        
                        assert.equals(#norGate.inputs, 0)
                        assert.equals(#norGate.outputs, 0)
                        assert.equals(norGate.delay, delay)
                        assert.equals(norGate.numberOfInputs, numberOfInputs)
                        assert.equals(norGate.type, GateInfo.NOR.type)
                    end
                )
                it(
                    "do_logic() should call simulate_logic()",
                    function()
                        local norGate = Nor(2, 1)
                        GateHelper.add_input_wire(norGate, Wire(1, LogicStates.UNKNOWN))
                        GateHelper.add_input_wire(norGate, Wire(2, LogicStates.UNKNOWN))
                        GateHelper.add_output_wire(norGate, Wire(3, LogicStates.UNKNOWN))
                        spy.on(norGate, "simulate_logic")
                        norGate:do_logic()
                
                        assert.spy(norGate.simulate_logic).was.called()
                    end
                )
            end
        )
        describe(
            "logic -",
            function()
                it(
                    "should have false outputs if any input is true",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local norGate = Nor(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(norGate, Wire(j, LogicStates.FALSE))
                            end
                            GateHelper.add_input_wire(norGate, Wire(i, LogicStates.TRUE))
                            GateHelper.add_output_wire(norGate, Wire(i + 1, LogicStates.UNKNOWN))

                            norGate:do_logic()
                            assert.is_true(norGate.outputs[1].state == LogicStates.FALSE)
                        end
                    end
                )
                it(
                    "should have true outputs if all inputs are false",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local norGate = Nor(i, 1)
                            for j = 1, i do
                                GateHelper.add_input_wire(norGate, Wire(j, LogicStates.FALSE))
                            end
                            GateHelper.add_output_wire(norGate, Wire(i + 1, LogicStates.UNKNOWN))

                            norGate:do_logic()
                            assert.is_true(norGate.outputs[1].state == LogicStates.TRUE)
                        end
                    end
                )
                it(
                    "should have false outputs if any input is true, even if other inputs are unknown",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local norGate = Nor(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(norGate, Wire(j, LogicStates.UNKNOWN))
                            end
                            GateHelper.add_input_wire(norGate, Wire(i, LogicStates.TRUE))
                            GateHelper.add_output_wire(norGate, Wire(i + 1, LogicStates.UNKNOWN))

                            norGate:do_logic()
                            assert.is_true(norGate.outputs[1].state == LogicStates.FALSE)
                        end
                    end
                )
                it(
                    "should have unknown outputs if any input is unknown and all other inputs are false",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local norGate = Nor(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(norGate, Wire(j, LogicStates.FALSE))
                            end
                            GateHelper.add_input_wire(norGate, Wire(i, LogicStates.UNKNOWN))
                            GateHelper.add_output_wire(norGate, Wire(i + 1, LogicStates.UNKNOWN))

                            norGate:do_logic()
                            assert.is_true(norGate.outputs[1].state == LogicStates.UNKNOWN)
                        end
                    end
                )
            end
        )
    end
)
