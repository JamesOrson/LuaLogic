local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"

local info = GateInfo.NOR

local Nor = {}

setmetatable(
    Nor,
    {
        __call = function (self, numberOfInputs, delay)
            o = {}
            setmetatable(o, self)
            self.__index = self
            o:initialize(numberOfInputs, delay)
            return o
        end
    }
)

function Nor:initialize(numberOfInputs, delay)
    self.delay = delay
    self.inputs = {}
    self.numberOfInputs = numberOfInputs
    self.outputs = {}
    self.type = info.type
end

function Nor:simulate_logic()
    if self.type ~= info.type or #self.inputs ~= self.numberOfInputs then
        return nil
    end

    local hasUnknown = false
    for _, wire in ipairs(self.inputs) do
        if wire.state == LogicStates.TRUE then
            return LogicStates.FALSE
        elseif wire.state == LogicStates.UNKNOWN then
            hasUnknown = true
        end
    end
    if hasUnknown then
        return LogicStates.UNKNOWN
    end
    return LogicStates.TRUE
end

function Nor:do_logic()
    local result = self:simulate_logic()
    for _, output  in ipairs(self.outputs) do
        output.state = result
    end
end

return Nor