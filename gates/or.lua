local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"

local info = GateInfo.OR

local Or = {}

setmetatable(
    Or,
    {
        __call = function (self, numberOfInputs, delay)
            o = {}
            setmetatable(o, self)
            self.__index = self
            o:initialize(numberOfInputs, delay)
            return o
        end
    }
)

function Or:initialize(numberOfInputs, delay)
    self.delay = delay
    self.inputs = {}
    self.numberOfInputs = numberOfInputs
    self.outputs = {}
    self.type = info.type
end

function Or:simulate_logic()
    if self.type ~= info.type or #self.inputs ~= self.numberOfInputs then
        return nil
    end

    local hasUnknown = false
    for _, wire in ipairs(self.inputs) do
        if wire.state == LogicStates.TRUE then
            return LogicStates.TRUE
        elseif wire.state == LogicStates.UNKNOWN then
            hasUnknown = true
        end
    end
    if hasUnknown then
        return LogicStates.UNKNOWN
    end
    return LogicStates.FALSE
end

function Or:do_logic()
    local result = self:simulate_logic()
    for _, output  in ipairs(self.outputs) do
        output.state = result
    end
end

return Or