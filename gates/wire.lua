local LogicStates = require "constants.logic_states"

local Wire = {}

setmetatable(
    Wire,
    {
        __call = function (self, id, state)
            o = {}
            setmetatable(o, self)
            self.__index = self
            o:initialize(id, state)
            return o
        end
    }
)

function Wire:initialize(id, state)
    if state == nil then
        state = LogicStates.UNKNOWN
    end
    self.id = id
    self.inputGate = nil
    self.outputGate = nil
    self.state = state
    self.history = ""
end

return Wire