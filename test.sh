#! /bin/bash

busted --verbose --coverage $@
if [ $? != 0 ]
then
    exit $?
fi
luacov