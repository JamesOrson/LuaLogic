local Api = require "api"

local function run_simulation(arg, outputFileName)
    if #arg < 1 then
        error("Please provide a circuit file name")
    end
    local circuitId = Api.load_circuit_from_file(arg[1])
    Api.begin_simulation(circuitId)
    Api.advance_circuit_simulation(circuitId, 8)
    Api.print_gates(circuitId)
end

local function run_simulation_error_handler(err)
    print("Error in run_simulation(): " .. err)
end

local gates = {}
local wires = {}
xpcall(run_simulation, run_simulation_error_handler, arg)